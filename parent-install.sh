#*******************************************************************************
# Copyright (c) 2014, 2015 Quinlan Jung.
# All rights reserved.
#*******************************************************************************
#!/usr/bin/env bash

# If Mac osx, set JAVA_HOME to custom jdk 1.7 path
OP_SYSTEM=$(uname -a | awk '{print $1}')
if [ "$OP_SYSTEM" = "Darwin" ]; then
	echo "OSX platform detected"
    export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_60.jdk/Contents/Home/
elif [ "$OP_SYSTEM" = "Linux" ]; then
	echo "Linux platform detected"
	export JAVA_HOME=/usr/java/jdk1.7.0_79
else
	echo "Platform $OP_SYSTEM not supported. Exiting."
	exit 1
fi

# Loop over all sibling directories to run install.sh if it exists 
for dir in ../*/
do
    dir=${dir%*/}
    
    if [ ! -f ../${dir##*/}/install.sh ]; then
        echo "install.sh not found in ${dir##*/}"
    else
        echo "install.sh found in $dir. Installing."
        cd $dir
        ./install.sh
        cd -
    fi
done

# Make this project usable in eclipse
mvn eclipse:eclipse

# Clean the previous build, if it exists
mvn clean

# Package all source code into a jar
mvn package